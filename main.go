package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Hello, World!")
	dt := time.Date(2022, time.March, 06, 0, 0, 0, 0, time.UTC)
	fmt.Printf("N'%sT00:00:00.000',", dt.Format("2006-01-02"))
}
