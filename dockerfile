FROM golang:latest
MAINTAINER Software Engineer
LABEL version="1.0"

WORKDIR /app

COPY go.mod ./
RUN go mod tidy

COPY *.go ./

RUN go build -o /main

CMD [ "/main" ]